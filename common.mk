# common.mk contains common targets to build services DO NOT EDIT!!!
#
# You can use tool/mkfixer to manage updating common.mk across many repositories.
#
#  AWS credentials rely on profiles. Specify correct profile before calling.
# 	Example: $ AWS_DEFAULT_REGION=us-east-1 AWS_PROFILE=k8sdev_root make linux-build docker helm
# 	Example: $AWS_SECRET_ACCESS_KEY=+lsllsls AWS_ACCESS_KEY_ID=adfasfdsafasdf AWS_DEFAULT_REGION=us-east-1  make linux-build docker helm
#
# DEPLOYING:
# minikube
#
# make helm-deploy will deploy to minikube same as any context. There is a convenience target called make local
# that will force a new build tag. Run with FORCE=true make local.
#
# aws
#
# The choice of where to deploy is driven by EKS_CLUSTERNAME and CLUSTER_NAME.
#	If EKS_CLUSTERNAME is set it will by default overwrite CLUSTER_NAME with EKS_CLUSTERNAME. This allows to deploy
#	by only specifying cluster.
#
#	$ EKS_CLUSTERNAME=second-bluzone-io make helm-deploy
#
# When working locally CLUSTER_NAME will be minikube and deploy to local minikube cluster
#
#	For more on deploying see [predicate](https://bitbucket.org/bluvision-cloud/predicate)

# This file should only be edited in goci/common.mk. Individual repos should
# modify targets as needed via inclusion.
#
# This file should be present in your repo.
# You can download common.mk from goci repo:
# https://bitbucket.org/bluvision-cloud/goci/raw/ab7b13618b828a8520efe04fafc490e7ad179d8f/common.mk

# This file requires see goci repo for links to install these
#1.  [jq] (https://stedolan.github.io/jq/download/)
#2. [docker] (https://docs.docker.com/install/)
#3. [kubectl] (https://kubernetes.io/docs/tasks/tools/install-kubectl/)
#4. [helm] (https://docs.helm.sh/using_helm/#installing-helm)

# Get the package root.. ie. bitbucket.org/bluvision-cloud/sunstone

# List of packages to generate docs for
# This list should only be the package.. ie mypckg not /bitbucket/cloud/mypkg
ifndef DOC_PKGS
DOC_PKGS:=
endif
# Root src directory
ifndef SRC_DIR
SRC_DIR := ./
endif
ABSOLUTE_SRC_DIR := $(realpath $(SRC_DIR))
ifndef DOCKER_CI_CTX
DOCKER_CI_CTX:=.
endif
ifndef PACKAGE_ROOT
PACKAGE_ROOT= $(shell  echo $(ABSOLUTE_SRC_DIR) | sed -e "s,^$(GOPATH)/src/,,")
endif
# Name of our go binary file. ex. main, cmd/service/service
ifndef BIN_PATH
BIN_PATH :=$(shell basename `pwd`)
endif

# basename for bitbucket repo
ifndef BITBUCKET_REPO_SLUG
BITBUCKET_REPO_SLUG :=$(shell basename $(ABSOLUTE_SRC_DIR))
endif

# repo to push to
ifndef REPO_NAME
REPO_NAME:=$(BITBUCKET_REPO_SLUG)
endif

# When using accounts assuming role we need to target correct registry
# This is account who owns the registry
ifndef ECR_REGISTRY_ID
ECR_REGISTRY_ID :=606323759582
endif

ORG_ID:=o-3v4hvm6wv1
ROOT_ROLE:=arn:aws:iam::$(ECR_REGISTRY_ID):role/ProdAccess

# Name of the registry
ifndef ECR_REGISTRY
ECR_REGISTRY :=$(ECR_REGISTRY_ID).dkr.ecr.us-east-1.amazonaws.com
endif

ifndef ECR_REGION
ECR_REGION :=us-east-1
endif

# when operating outside a git repo this tag is used.s
NO_GIT_TAG:=0.0.0-0-nocommitlocal
NO_COMMIT:=nocommitlocal
ifndef GIT_TAG
# When no git repo is setup this will be fatal:
GIT_TAG=$(shell git describe 2>&1 | cut -f1 -d " " | sed -e "s/^v//")
GIT_COMMIT=$(shell git rev-parse HEAD)
ifeq "$(GIT_TAG)" "fatal:"
GIT_TAG:=$(NO_GIT_TAG)
GIT_COMMIT:=$(NO_COMMIT)
endif
endif
ifndef GIT_BRANCH
ifeq "$(GIT_TAG)" "$(NO_GIT_TAG)"
# when no git set it local
GIT_BRANCH=local
else
# clean the branch of prefixes:
#   hotfix/
#   feature/
#   release/
# clean period
# remove dash
# output max 8 chars
# example:
#   BCP-450549-some-long-really-long-branch-name => bcp45054
#   hotfix/v1.0.3-rc1someverylongthing => hv103rc1
#   feature/testpipe => ftestpip
GIT_BRANCH:=$(shell git rev-parse --abbrev-ref HEAD)
endif
endif


# Values we extracted from branch name: release/dev/bzdev/bzdev
B_PROCEDURE:=$(shell echo $(GIT_BRANCH) | cut -f1 -d "/")
B_PROFILE:=$(shell echo $(GIT_BRANCH) | cut -f2 -d "/")
B_KUBECONTEXT:=$(shell echo $(GIT_BRANCH) | cut -f3 -d "/")
NAMESPACE:=$(shell echo $(GIT_BRANCH) | cut -f4 -d "/")
ifeq "$(NAMESPACE)" ""
CLEAN_BRANCH:=$(shell echo $(GIT_BRANCH) | sed  -E -e "s~^(.{1}).*\/(.*)$$~\1\2~g" | sed  -E -e "s/\.//g" | sed  -E -e "s/-//g" | sed  -E -e "s~^(.{8}).*$$~\1~g" | tr '[:upper:]' '[:lower:]')
else
CLEAN_BRANCH:=$(shell echo $(NAMESPACE) | sed  -E -e "s~^(.{1}).*\/(.*)$$~\1\2~g" | sed  -E -e "s/\.//g" | sed  -E -e "s/-//g" | sed  -E -e "s~^(.{8}).*$$~\1~g" | tr '[:upper:]' '[:lower:]')
endif

CLEAN_BRANCH:=$(shell echo $(GIT_BRANCH) | sed  -E -e "s~^(.{1}).*\/(.*)$$~\2~g" | sed  -E -e "s/\.//g" | sed  -E -e "s/-//g" | sed  -E -e "s~^(.{8}).*$$~\1~g" | tr '[:upper:]' '[:lower:]')
BUILD_DATE := $(shell date -u +%FT%T%z)

ifeq "$(GIT_TAG)" "$(NO_GIT_TAG)"
IMAGE_SHORT_TAG=0.0.0
CURRENT_RELEASE=0.0.0
CURRENT_RELEASE_COMMIT=0.0.0
else
# Image short tag represents just the version tag 0.0.0
CURRENT_RELEASE=$(shell git describe --abbrev=0)
CURRENT_RELEASE_COMMIT=$(shell git rev-parse $(CURRENT_RELEASE))
IMAGE_SHORT_TAG=$(shell git describe --abbrev=0 | sed -e "s/^v//")
endif
# Remainder of image short
MAJOR=$(shell echo $(IMAGE_SHORT_TAG) | sed -E -e "s/^(.*)\..*\..*$$/\1/g")
MINOR=$(shell echo $(IMAGE_SHORT_TAG) | sed -E -e "s/^.*\.(.*)\..*$$/\1/g")
# Patch we can auto increment
PATCH =$(shell echo $(IMAGE_SHORT_TAG) | sed -E -e "s/^(.*\.)//g")
MAJOR_MINOR:=$(MAJOR).$(MINOR)
NEXT_RELEASE_VERSION:=$(shell MV=$(MINOR); echo v$(MAJOR).$$((MV+1)).0)
NEXT_RELEASE_BRANCH:=release/$(NEXT_RELEASE_VERSION)
NEXT_RELEASE_COMMIT:=$(shell git log -n 1 $(GIT_BRANCH) --pretty=format:"%H")

NEXT_HOTFIX:=$(shell MV=$(PATCH); echo $(MAJOR).$(MINOR).$$((MV+1)))
NEXT_HOTFIX_VERSION:=$(shell echo v$(NEXT_HOTFIX))
NEXT_HOTFIX_BRANCH:=hotfix/$(NEXT_HOTFIX_VERSION)
# Image Tag will be used for tagging our containers.. ie 0.0.0-14-g7479f6d-master
# This is here so our 'prerelease' is from new is lower based on commit
ifeq ($(findstring -,$(GIT_TAG)),-)
    # Found
else
GIT_TAG:=$(GIT_TAG)-0
endif

# make sure this tag our tags are actually semver compliant so they sort correctly
STRIP_TAG :=$(shell echo $(GIT_TAG) | sed -E -e "s/$(IMAGE_SHORT_TAG)//")
REAL_TAG :=$(shell echo $(STRIP_TAG) | sed -E -e "s/-/./g")

# This is the branch that will always point at latest
MARKER_BRANCH_NAME:=latest
#GIT_TAG:=$(IMAGE_SHORT_TAG)-$(GIT_BRANCH)$(REAL_TAG)
#
# To override the tag that will be used
# 		make docker TAG=latest
#
# To specify an exact tag and have the version ($IMAGE_SHORT_TAG) pushed as well
# 		make docker IMAGE_TAG=1.0.0
ifeq "$(GIT_BRANCH)" "$(MARKER_BRANCH_NAME)"
# When creating artifacts on marker branch we have released so use real version
IMAGE_TAG:=$(IMAGE_SHORT_TAG)
else
IMAGE_TAG:=$(IMAGE_SHORT_TAG)-$(CLEAN_BRANCH)$(REAL_TAG)
endif
# Required for release doc interaction with bitbucket and jira
# You should provide these values in your environment
# when in bitbucket they will be provided
ifndef BB_LOGIN
BB_LOGIN:=notvalid
endif

ifndef BB_PASSWORD
BB_PASSWORD:=notvalid
endif
JIRA_TAG:=BCP-

ifdef TAG
IMAGE_TAG:=$(TAG)
endif

# If force is used it will use the date to touch the tag so we get a new deployment
ifdef FORCE
F_DATE:=$(shell date -u +%Y%m%d%H%M%S)
IMAGE_TAG:=$(IMAGE_TAG)-F-$(F_DATE)
endif

# Who is building this and what build number did they give us
BUILDER :=local
ifdef bamboo_buildNumber
BUILD_NUMBER =$(bamboo_buildNumber)
BUILDER =bamboo
endif
ifdef BITBUCKET_BUILD_NUMBER
BUILD_NUMBER=$(BITBUCKET_BUILD_NUMBER)
BUILDER=bitbucket
endif
ifndef BUILD_NUMBER
BUILD_NUMBER=0
endif

# This is so we can use
LEGACY_BRANCH:=$(shell echo $(GIT_BRANCH) | sed -E -e "s,/,-,g")
LEGACY_TAG:=$(LEGACY_BRANCH)-$(BUILD_NUMBER)-$(NEXT_RELEASE_COMMIT)

# The complete Sem Ver we will use for auto version
SEM_VER := $(GIT_TAG)+$(BUILDER)_$(BUILD_NUMBER).$(BUILD_DATE)
LD_FLAGS :=-ldflags="-s -w -X bitbucket.org/bluvision-cloud/kit/version.version=$(SEM_VER)"

ifndef PROTO_PKG
PROTO_PKG:=none
endif
ifndef SWAGGER_OUT
SWAGGER_OUT:=$(PROTO_PKG)
endif


GATEWAY_VERSION=$(shell cat $(ABSOLUTE_SRC_DIR)/go.mod | grep grpc-gateway | cut -f2 | cut -d ' ' -f2)

PROTO_INCLUDE=-I /usr/local/include \
								-I . -I $(GOPATH)/src \
								-I ../../../ \
								-I $(GOPATH)/src/$(PACKAGE_ROOT)/vendor/ \
								-I $(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
								-I $(GOPATH)/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@$(GATEWAY_VERSION)/third_party/googleapis

# CLUSTER_NAME is a name for kube cluster we deploy to
# If EKS_CLUSTERNAME is defined we will use eks_name as clustername as we
# should be deploying to a cluster.
ifndef CLUSTER_NAME
ifdef EKS_CLUSTERNAME
CLUSTER_NAME=$(EKS_CLUSTERNAME)
else
CLUSTER_NAME=minikube
endif
endif

ifndef ROOT_CERTS
ROOT_CERTS :=./resources/certs
endif

ifndef ROOT_DEPLOY
ROOT_DEPLOY :=./resources/deploy
endif

ifndef ROOT_DOCS
ROOT_DOCS := ./resources/docs/content/releases
endif

# helm variables for package mngmt
ifndef DEPLOY_HELM_DIR
DEPLOY_HELM_DIR := $(ROOT_DEPLOY)/helm
endif

DEPLOY_AWS_DIR := $(ROOT_DEPLOY)/aws

# set a different base for helm that is not the repo
ifndef HELM_BASE
HELM_BASE:=$(REPO_NAME)
endif
# where to find env values.yaml(s)
ifndef HELM_ENV_DIR
HELM_ENV_DIR:= $(DEPLOY_HELM_DIR)/env
endif

ifndef INFRA_CONFIG
INFRA_CONFIG:=service.yml
endif

# Config is override to force a directory to find values.yaml when deploying
ifndef CONFIG
CONFIG:=none
endif

ifndef HELM_BUCKET
HELM_BUCKET:=bluvision-release
endif

HELM_ARTIFACT :=$(HELM_BASE)-$(IMAGE_TAG).tgz

# Every branch gets its own repo for charts so our versioning will not cross as we
# treat branch as deployment lines.
BRANCH_HELM_REPO_NAME:=bluzone-charts-$(CLEAN_BRANCH)
BRANCH_HELM_REPO=charts/$(CLEAN_BRANCH)
BRANCH_HELM_REPO_URL:=s3://$(HELM_BUCKET)/$(BRANCH_HELM_REPO)

# Master is special and will be used to fallback when no other charts found.
MASTER_HELM_REPO_NAME:=bluzone-charts
ifndef MASTER_HELM_REPO
MASTER_HELM_REPO=charts/master
endif
MASTER_HELM_REPO_URL:=s3://$(HELM_BUCKET)/$(MASTER_HELM_REPO)

RELEASE_HELM_REPO_NAME:=bluzone-charts-release
RELEASE_HELM_REPO=charts/bzprod
RELEASE_HELM_REPO_URL:=s3://$(HELM_BUCKET)/$(RELEASE_HELM_REPO)

# Depending on branch and lifecycle; we will put our charts in different repos.
TARGET_HELM_REPO_URL:=$(BRANCH_HELM_REPO_URL)
TARGET_HELM_REPO:=$(BRANCH_HELM_REPO_NAME)
TARGET_HELM_DIR:=$(BRANCH_HELM_REPO)
DEPENDS_HELM_REPO_NAME:=bluzone-charts-depends
ifndef DEPENDS_HELM_REPO
DEPENDS_HELM_REPO:=$(BRANCH_HELM_REPO)
endif
DEPENDS_HELM_REPO_URL:=s3://$(HELM_BUCKET)/$(DEPENDS_HELM_REPO)

HELM_REQ_PATH=$(DEPLOY_HELM_DIR)/$(HELM_BASE)/requirements.yaml
HELM_RELEASE_NAME=$(HELM_BASE)-$(NAMESPACE)


BOM_TAG := $(IMAGE_TAG)-$(HELM_BASE)-bom
ifndef BOM_OUT_DIR
BOM_OUT_DIR:=/tmp
endif
BOM_HELM_DIR=$(BOM_OUT_DIR)/$(BOM_REPO_SLUG)/resources/deploy/helm
BOM_BRANCH=master
BOM_BASE=bluzone
BOM_REPO_SLUG=bom
BOM_REPO=https://bitbucket.org/bluvision-cloud/$(BOM_REPO_SLUG)
BOM_REQ_PATH=$(BOM_HELM_DIR)/$(BOM_BASE)/requirements.yaml
BOM_ARTIFACT :=$(BOM_BASE)-$(BOM_TAG).tgz
ifndef NAMESPACE
NAMESPACE:=$(CLEAN_BRANCH)
endif
BOM_RELEASE_NAME:= $(BOM_BASE)-$(NAMESPACE)

# ENV_HOSTNAME aligns with an aws account dev/test/prod etc...
# 	example would be newenv.bluzone.io where the actual hostname
# 	would be master.newenv.bluzone.io
ifndef ENV_HOSTNAME
ifeq "$(CLUSTER_NAME)" "minikube"
ENV_HOSTNAME:=local.bluzone.io
IS_AMAZON=false
else
IS_AMAZON=true
ENV_HOSTNAME:=$(CLUSTER_NAME)
endif
endif


# 	full hostname would be master-newenv.bluzone.io
ifndef CLUSTER_HOSTNAME
ifeq "$(CLUSTER_NAME)" "minikube"
CLUSTER_HOSTNAME:=$(NAMESPACE)-$(ENV_HOSTNAME)
else
CLUSTER_HOSTNAME:=$(NAMESPACE)-$(ENV_HOSTNAME)
endif
endif

CLEAN_ENV_HOSTNAME:=$(shell echo $(ENV_HOSTNAME) | sed -E -e "s/\./-/g")
CLEAN_HOSTNAME:=$(shell echo $(CLUSTER_HOSTNAME) | sed -E -e "s/\./-/g")
# The resolved directory to grab our helm values.yaml to use
TARGET_HELM_CONFIG:=$(CLEAN_BRANCH)
ifeq ($(CONFIG),none)
ifeq ($(CLUSTER_NAME),minikube)
TARGET_HELM_CONFIG:=minikube
else
TARGET_HELM_CONFIG:=$(CLEAN_HOSTNAME)
endif
else
TARGET_HELM_CONFIG:=$(CONFIG)
endif

# If we don't have keys... try getting them from a profile
ifndef AWS_PROFILE
AWS_PROFILE=default
endif

ifndef AWS_ACCESS_KEY_ID
AWS_ACCESS_KEY_ID=$(shell aws configure --profile $(AWS_PROFILE) get aws_access_key_id | xargs)
endif
ifndef AWS_SECRET_ACCESS_KEY
AWS_SECRET_ACCESS_KEY=$(shell aws configure --profile $(AWS_PROFILE) get aws_secret_access_key | xargs)
endif
HISTORY_URL="https://api.bitbucket.org/2.0/repositories/bluvision-cloud/$(REPO_NAME)/commits/?exclude=$(CURRENT_RELEASE_COMMIT)&include=$(NEXT_RELEASE_COMMIT)&page=1&pagelen=30"

ORIG_AWS_ACCESS_KEY_ID:=$(AWS_ACCESS_KEY_ID)
ORIG_AWS_SECRET_ACCESS_KEY:=$(AWS_SECRET_ACCESS_KEY)

# Where to load our legacy infra env from
INFRA_ENV_DIR=$(GIT_BRANCH)
ifdef INFRA_ENV_PREFIX
INFRA_ENV_DIR=$(INFRA_ENV_PREFIX)_$(GIT_BRANCH)
endif

ADDITIONAL_REPO_NAME=$(REPO_NAME)
ifeq ($(findstring bz/,$(REPO_NAME)),bz/)
ADDITIONAL_REPO_NAME=$(HELM_BASE)
endif

ZONE_DNS_NAME=bluzone.io

# CLOUD_HOST is the host that gets passed to portal to default connect blufis
ifndef CLOUD_HOST
ifeq ($(CLUSTER_NAME),minikube)
CLOUD_HOST=$(shell ifconfig | grep "inet " | grep -m1 -v 127.0.0.1 | cut -d\  -f2)
else
# set to blank value
CLOUD_HOST=""
endif
endif

# when set true we will not check route53 entries
ifndef SKIP_DNS
SKIP_DNS=false
endif

CALLER_ID:=$(shell aws sts get-caller-identity | jq -r '.Account')

REGION:=us-east-1
ifdef AWS_DEFAULT_REGION
REGION=$(AWS_DEFAULT_REGION)
else
REGION=$(shell aws configure --profile $(AWS_PROFILE) get region | xargs)
endif

# Bucket we use to store environment assets,templates etc.. This is NOT the general
# storage for running bluzone.
#%s-cf-state-store-%s
ENVIRONMENT_BUCKET_NAME:=$(CALLER_ID)-cf-state-store-$(REGION)
ENV_PARAM_TMPL:=$(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.tmpl.yaml
NAMESPACE_CF_NAME:=$(NAMESPACE)-$(EKS_CLUSTERNAME)
STORAGE_RETENTION:=Delete
# no deletes in prod account
PROD_ACCOUNT_ID:=258974982883
IS_PROD:=false
ifeq ($(CALLER_ID),$(PROD_ACCOUNT_ID))
IS_PROD:=true
STORAGE_RETENTION:=Retain
endif
# This should be one of the extensions you want to use.
# One of goout(default),gogofast_out,gogofaster_out,gogoslick_out
# Find more here https://github.com/gogo/protobuf
# Make sure you have the binary for your choice installed... via link above.
ifndef PROTOC_EXTENSION
PROTOC_EXTENSION=go_out
endif


# Template to use for creating new environments
ifndef STARTER_TEMPLATE
STARTER_TEMPLATE :=values-starter-template.yaml
endif

ifndef ENAME
ENAME := bzdev
endif

.PHONY: docker test resources

all: build

# This is convenience to send to local docker.
# ideally run this with FORCE=true make local
local: linux-build  docker-build docker-tag helm helm-deploy

#  no ensure as mods are used
mod-release: info linux-build compress release-artifacts

# Alias to mod-release as we no longer support dep!
release: mod-release

# build and store our release artifacts
release-artifacts: docker helm

# Build a linux binary in a ci container and push to repo
release-ci: docker-ci docker-tag docker-push

buil%:
	go build -v -i $(LD_FLAGS) -o $(BIN_PATH) $(BIN_PATH).go

arm-buil%:
	CGO_ENABLED=0 GOOS=linux GOARCH=arm go build $(LD_FLAGS) -o $(BIN_PATH) $(BIN_PATH).go

linux-buil%:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build $(LD_FLAGS) -o $(BIN_PATH) $(BIN_PATH).go

# compress will compress our executable. Sometimes you will find strange issues where the binary produced
# from upx is not executable... You should not use compress then:) Im not sure what magic makes it work / not work yet. Most times it works.
# compress also has higher success for linux amd64 and always works for arm.
compress:
	upx -9 $(BIN_PATH)

test:
	go test $$(go list ./... | grep -v /vendor/)

#########################
# helm
#########################

# make sure we have a route53-zone for a new host we will create. This is necessary as short term
# we can't do subdomain routing and have to go back to root each time.
route53-zone:
	@ if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
		exit; \
	fi; \
	if [[ $(SKIP_DNS) = "true" ]]; then \
		exit; \
	fi; \
	echo "-- looking up hosted zone_ids for $(CLUSTER_HOSTNAME)"; \
	zone_id=$$(aws route53 list-hosted-zones  | jq -r ".HostedZones[] | select(.Name==\"$(CLUSTER_HOSTNAME).\") | .Name"); \
	case "$$zone_id" in \
		*$(CLUSTER_HOSTNAME)*) \
			echo "-- route53 zone already exists for $(CLUSTER_HOSTNAME). not modifying"; \
		;; \
		*) \
			echo "-- route53 creating zone for $(CLUSTER_HOSTNAME). "; \
			env_servers=$$(aws route53 create-hosted-zone --name $(CLUSTER_HOSTNAME)  --caller-reference $$(uuidgen) | jq -r '.DelegationSet.NameServers'); \
			server0=$$(echo $$env_servers | jq -r '.[0]'); \
			server1=$$(echo $$env_servers | jq -r '.[1]'); \
			server2=$$(echo $$env_servers | jq -r '.[2]'); \
			server3=$$(echo $$env_servers | jq -r '.[3]'); \
			echo "\
			{\
			  \"Comment\": \"Delegating to env $(ENV_HOSTNAME) for domain $(CLUSTER_HOSTNAME)\",\
			  \"Changes\": [\
				{\
				  \"Action\": \"CREATE\", \
				  \"ResourceRecordSet\": { \
					\"Name\": \"$(CLUSTER_HOSTNAME)\", \
					\"Type\": \"NS\",\
					\"TTL\": 300,\
					\"ResourceRecords\": [\
					  {\"Value\": \"$$server0\"},\
					  {\"Value\": \"$$server1\"},\
					  {\"Value\": \"$$server2\"},\
					  {\"Value\": \"$$server3\"}\
					]\
				  }\
				}\
			  ]\
			}\
			" > subdomain.json; \
		   current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
		   case "$$current_acct" in \
				*$(ECR_REGISTRY_ID)*) \
					echo "Already root nothing to do."\
				;; \
				*) \
				credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
				export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
				export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
				export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
				echo "-- assumed root access"; \
				;; \
			esac; \
			parent_zone_id=$$(aws route53 list-hosted-zones  | jq -r ".HostedZones[] | select(.Name==\"$(ZONE_DNS_NAME).\") | .Id"); \
			echo "Zone Id $$parent_zone_id for $(ZONE_DNS_NAME)"; \
			aws route53 change-resource-record-sets --hosted-zone-id $$parent_zone_id --change-batch file://subdomain.json; \
			rm -Rf subdomain.json; \
		;; \
	esac;

# this will make sure there is a public route53 entry for helm-deploy pointing at ingress.
# convention is {branch: master}-{env:dev}.bluzone.io. Its assumed that account
# has control over {env:dev}.bluzone.io as our env setup would make it so.
# This will not overwrite if a recordset is found.
route53-host: route53-zone
	@ if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
		exit; \
	fi; \
	if [[ $(SKIP_DNS) = "true" ]]; then \
		exit; \
	fi; \
	echo "-- looking up hosted zone_ids for $(CLUSTER_HOSTNAME)"; \
	zone_id=$$(aws route53 list-hosted-zones  | jq -r ".HostedZones[] | select(.Name==\"$(CLUSTER_HOSTNAME).\") | .Id"); \
	echo $$zone_id; \
	existing_record=$$(aws route53 list-resource-record-sets --hosted-zone-id $$zone_id | jq -r '.ResourceRecordSets[] | select((.Name=="$(CLUSTER_HOSTNAME).") and .Type=="A") | .Name'); \
	echo "-- checking if record set exists for: $(CLUSTER_HOSTNAME)"; \
	case "$$existing_record" in \
		*$(CLUSTER_HOSTNAME)*) \
			echo "-- route53 already exists for $(CLUSTER_HOSTNAME). not modifying"; \
		;; \
		*) \
			lb_names=$$(aws elb describe-load-balancers | jq -r '.LoadBalancerDescriptions[].LoadBalancerName'); \
			for lb in $$lb_names; do \
				echo "-- found load balancer $$lb"; \
				tag=$$(aws elb describe-tags --load-balancer-names $$lb  | jq -r '.TagDescriptions[] | select(.Tags[].Value == "istio-system/istio-ingressgateway" and (.Tags[].Key=="kubernetes.io/cluster/$(ENV_HOSTNAME)" and .Tags[].Value=="'owned'")) | .LoadBalancerName'); \
				if ! [[ -z $$tag ]]; then \
					echo "-- $$lb is the istio ingress lb we will create dns for: $(CLUSTER_HOSTNAME)"; \
					lb_desc=$$(aws elb describe-load-balancers --load-balancer-names $$lb); \
					elb_zone=$$(echo $$lb_desc | jq -r '.LoadBalancerDescriptions[].CanonicalHostedZoneNameID'); \
					lb_dns=$$(echo $$lb_desc | jq -r '.LoadBalancerDescriptions[].DNSName'); \
					echo "-- zone Id $$zone_id for $(ENV_HOSTNAME)"; \
					echo " \
					{\
					  \"Comment\": \"Update domain\",\
					  \"Changes\": [\
						{\
						  \"Action\": \"UPSERT\",\
						  \"ResourceRecordSet\": {\
							\"Name\": \"$(CLUSTER_HOSTNAME)\",\
							\"Type\": \"A\",\
							\"AliasTarget\": {\
								\"DNSName\": \"$$lb_dns\",\
								\"HostedZoneId\": \"$$elb_zone\",\
								\"EvaluateTargetHealth\": false\
							}\
						  }\
						}\
					  ]\
					}\
					" > domain.json;\
					aws route53 change-resource-record-sets --hosted-zone-id $$zone_id --change-batch file://domain.json; \
					rm -Rf domain.json; \
				fi; \
			done; \
		;; \
	esac;


# package and deploy a helm chart
helm: ensure-release-repo ensure-master-repo ensure-branch-repo helm-add helm-replace helm-package helm-copy-artifact helm-repo-merge

# add helm repos
helm-add:
	@ res=$$(helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/)
	@ res=$$(helm repo add $(BRANCH_HELM_REPO_NAME) $(BRANCH_HELM_REPO_URL))
	@ res=$$(helm repo add bluzone-charts-branch $(BRANCH_HELM_REPO_URL))
	@ res=$$(helm repo add $(MASTER_HELM_REPO_NAME) $(MASTER_HELM_REPO_URL))
	@ res=$$(helm repo add bluzone-charts $(MASTER_HELM_REPO_URL))
	@ res=$$(helm repo add $(RELEASE_HELM_REPO_NAME) $(RELEASE_HELM_REPO_URL))
	@ res=$$(helm repo add $(DEPENDS_HELM_REPO_NAME) $(DEPENDS_HELM_REPO_URL))

# Replace our placements if we are the bom itself creating a chart.
#this is similar to helm search... but much faster. helm search bluzone-charts-branch/$f
helm-replace:
	@ res=$$(helm repo up)
	@ if [ -f $(HELM_REQ_PATH).tmpl ]; then \
		cp $(HELM_REQ_PATH).tmpl $(HELM_REQ_PATH); \
		echo "-- Replacing repo dependency replacements"; \
		for f in $$(cfn-flip $(HELM_REQ_PATH).tmpl | jq -r '.dependencies[] .name'); \
        do \
        	res=$$(grep -m 1 "name: $$f" ~/.helm/repository/cache/$(DEPENDS_HELM_REPO_NAME)-index.yaml | xargs); \
        	case "$$res" in \
        		*name*) \
        			sed -i.bak "s/\(<repo-$$f>\)/"\"\@$(DEPENDS_HELM_REPO_NAME)"\"/g" $(HELM_REQ_PATH); \
        		;; \
        		*) \
        			shouldReplace=$$(grep "<repo-$$f>" $(HELM_REQ_PATH)); \
        			case "$$shouldReplace" in \
        				*repository*) \
        					echo "warning: $(MASTER_HELM_REPO_NAME) will be used for dependency $$f because a version in repo $(DEPENDS_HELM_REPO_NAME) was not found. To deploy branch version create branch and run make helm"; \
							sed -i.bak "s/\(<repo-$$f>\)/"\"\@$(MASTER_HELM_REPO_NAME)"\"/g" $(HELM_REQ_PATH); \
						;; \
					esac; \
        		;; \
        	esac; \
        done; \
		sed -i.bak "s/\(<version-.*>\)/"\"\>=0.0.0-0"\"/g" $(HELM_REQ_PATH); \
		rm -Rf $(HELM_REQ_PATH).bak; \
	fi;

# package up our chart
helm-package :
	@ res=$$(helm init --client-only)
	@ rm -Rf $(DEPLOY_HELM_DIR)/$(HELM_BASE)/charts/*
	@ rm -Rf $(DEPLOY_HELM_DIR)/*.tgz
	@ rm -Rf $(DEPLOY_HELM_DIR)/index.yaml
	@ rm -Rf $(DEPLOY_HELM_DIR)/update.yaml
	@ rm -Rf $(DEPLOY_HELM_DIR)/$(HELM_BASE)/requirements.lock
	@ current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
	case "$$current_acct" in \
		*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
		*) \
		credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
		export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
		export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
		export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
		echo "-- assumed root access"; \
		;; \
	esac; \
	res=$$(helm dep up $(DEPLOY_HELM_DIR)/$(HELM_BASE));
	@ echo "-- packaging helm chart at $(DEPLOY_HELM_DIR)/$(HELM_BASE) with chart version: $(IMAGE_TAG)"
	@ res=$$(helm package --app-version=$(IMAGE_TAG) --version=$(IMAGE_TAG) --destination=$(DEPLOY_HELM_DIR) $(DEPLOY_HELM_DIR)/$(HELM_BASE))

# deploy the chart to s3
helm-copy-artifact:
	@ current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
	case "$$current_acct" in \
		*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
		*) \
		credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
		export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
		export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
		export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
		echo "-- assumed root access"; \
		;; \
	esac; \
	res=$$(aws s3 cp $(DEPLOY_HELM_DIR)/$(HELM_ARTIFACT)  $(TARGET_HELM_REPO_URL)/$(HELM_ARTIFACT) --acl  bucket-owner-full-control)

# utility to merge our existing chart/index.yaml with our newly deployed artifact
helm-repo-merge:
	@ while true;\
	do \
		current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
		case "$$current_acct" in \
			*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
			*) \
			credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
			export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
			export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
			export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
			echo "-- assumed root access"; \
			;; \
		esac; \
		rm -Rf $(DEPLOY_HELM_DIR)/merge.lock; \
		res=$$(aws s3api get-object --bucket=$(HELM_BUCKET) --key=$(TARGET_HELM_DIR)/merge.lock $(DEPLOY_HELM_DIR)/merge.lock 2>&1); \
		case "$$res" in \
			*"NoSuchKey"* )  \
				echo "-- no lock file found. i will lock it"; \
				echo "$(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG)" >> $(DEPLOY_HELM_DIR)/merge.lock; \
				pres=$(aws s3api put-object --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(TARGET_HELM_DIR)/merge.lock --body $(DEPLOY_HELM_DIR)/merge.lock); \
				echo "creating lock at $(TARGET_HELM_DIR)/merge.lock with $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG)"; \
			;;  \
			* ) \
				lv=$$(cat $(DEPLOY_HELM_DIR)/merge.lock); \
				echo "-- cannot proceed as another $$lv process has locked it. I will wait and try again later."; \
				sleep 2; \
				continue;  \
			;; \
		esac; \
		res=$(aws s3api get-object --bucket=$(HELM_BUCKET) --key=$(TARGET_HELM_DIR)/merge.lock $(DEPLOY_HELM_DIR)/merge.lock); \
		lv=$$(cat $(DEPLOY_HELM_DIR)/merge.lock); \
		case "$$lv" in \
			"$(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG)" ) \
				echo "-- lock file is ok and will begin processing repo merge"; \
				res=$$(aws s3api get-object --bucket=$(HELM_BUCKET) --key=$(TARGET_HELM_DIR)/update.yaml $(DEPLOY_HELM_DIR)/update.yaml;) \
				helm repo index --merge=$(DEPLOY_HELM_DIR)/update.yaml --url=$(TARGET_HELM_REPO_URL) $(DEPLOY_HELM_DIR); \
				ETAG=$$(aws s3api put-object --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(TARGET_HELM_DIR)/update.yaml --body $(DEPLOY_HELM_DIR)/index.yaml | jq '.ETag'); \
				ETAG=$${ETAG:3:32}; \
				CPRES=$$(aws s3api copy-object --copy-source=$(HELM_BUCKET)/$(TARGET_HELM_DIR)/update.yaml --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(TARGET_HELM_DIR)/index.yaml --copy-source-if-match=$$ETAG 2>&1); \
				case "$$CPRES" in \
					*PreconditionFailed*) \
						echo "-- Merge attempt failed because update.yml was inconsistent. Trying again."; \
						sleep 2; \
						continue; \
					;;\
					*) \
						echo "-- Successfully deployed chart $(TARGET_HELM_REPO_URL) and merged repo"; \
					;; \
				esac; \
				echo "-- removing lock file as process is complete"; \
				aws s3api delete-object --bucket=$(HELM_BUCKET) --key=$(TARGET_HELM_DIR)/merge.lock; \
				rm -Rf $(DEPLOY_HELM_DIR)/merge.lock; \
				break; \
			;; \
			* ) \
				echo "-- $$lv process has locked this repo. will try again later." \
				sleep 2; \
				continue; \
			;; \
		esac; \
	done

# this is nice to be fresh; but if iterating this can slow you donw.
delete-namespace:
	kubectl delete namespace $(NAMESPACE)

# delete this helm deployed release
helm-delete: kube-set-context
	helm delete --purge $(HELM_RELEASE_NAME)

# You can override what file to use by passing INFRA_CONFIG
# You can override where to look for the env by passing INFRA_ENV_LOCATION
infra-deploy:
	source $(HELM_ENV_DIR)/$(INFRA_ENV_DIR)/env.sh && bz-infra --cmd=service --task=deploy --config=$(INFRA_CONFIG)
# we only need to do if minikube as aws does have regcreds
# helm will autocreate namespace but that to late.
init-namespace:
		@ CK=$$(kubectl get namespace $(NAMESPACE) -o=jsonpath='{.status.phase}'); \
		case "$$CK" in \
			*Active) \
				echo "namespace $(NAMESPACE) exists"; \
				break; \
			;; \
			*) \
				echo "lets create it"; \
				kubectl create namespace $(NAMESPACE); \
				if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
					kubectl -n kube-system delete pods $$(kubectl -n kube-system get pods -l name=registry-creds -o=jsonpath='{.items[0].metadata.name}'); \
					sleep 10; \
					kubectl get secret -n $(NAMESPACE) awsecr-cred; \
				fi; \
			;; \
		esac; \

helm-debug: helm-add kube-set-context
	@ res=$$(kubectl label namespace $(NAMESPACE) istio-injection=enabled --overwrite)
	@ current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
	case "$$current_acct" in \
		*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
		*) \
		credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
		export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
		export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
		export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
		echo "-- assumed root access"; \
		;; \
	esac; \
	res=$$(aws s3 cp s3://$(HELM_BUCKET)/$(TARGET_HELM_DIR)/$(HELM_ARTIFACT) $(DEPLOY_HELM_DIR)/$(HELM_ARTIFACT) --acl  bucket-owner-full-control); \
	VALUES_FILE=values.yaml; \
	if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
		if [ -f $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/userlocal_values.yaml ]; then \
		VALUES_FILE=userlocal_values.yaml; \
		echo "-- running in minikube and found a userlocal_values.yaml present. we will use for values"; \
		fi; \
	fi; \
	if [ -f $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE]; then \
		echo ""; \
		echo "-- Deploying using branch values from $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE"; \
		echo ""; \
		helm install --dry-run --debug $(DEPLOY_HELM_DIR)/$(HELM_BASE) --namespace=$(NAMESPACE) \
			--values=$(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE \
			--set global.isAmazon="$(IS_AMAZON)" \
			--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
			--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
			--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY) \
			--set global.hosts={$(CLUSTER_HOSTNAME)}; \
	elif [ -f $(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml ]; then \
		echo ""; \
		echo "-- Deploying using branch values from $(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml"; \
		echo ""; \
		helm install --dry-run --debug $(DEPLOY_HELM_DIR)/$(HELM_BASE) --namespace=$(NAMESPACE) \
			--values=$(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml \
			--set global.isAmazon="$(IS_AMAZON)" \
			--set global.hosts={$(CLUSTER_HOSTNAME)} \
			--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
			--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
			--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY); \
	else \
		if [ -f $(HELM_ENV_DIR)/values.yaml ]; then \
			echo ""; \
			echo "-- Deploying using root values from $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml"; \
			echo ""; \
			helm install --dry-run --debug $(DEPLOY_HELM_DIR)/$(HELM_BASE) --namespace=$(NAMESPACE)\
				--set global.isAmazon="$(IS_AMAZON)" \
				--values=$(HELM_ENV_DIR)/values.yaml \
				--set global.hosts={$(CLUSTER_HOSTNAME)} \
				--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
				--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
				--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY); \
		else \
			echo ""; \
			echo "-- Deploying using no values files as $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml was no present."; \
			echo ""; \
			helm install --dry-run --debug $(DEPLOY_HELM_DIR)/$(HELM_BASE) --namespace=$(NAMESPACE) \
				--set global.isAmazon="$(IS_AMAZON)" \
				--set global.hosts={$(CLUSTER_HOSTNAME)}  \
				--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
				--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
				--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY); \
		fi; \
	fi; \

# copies artifact to s3
copy-helm:
	@ current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
	case "$$current_acct" in \
		*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
		*) \
		credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
		export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
		export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
		export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
		echo "-- assumed root access"; \
		;; \
	esac; \
	res=$$(aws s3 cp s3://$(HELM_BUCKET)/$(TARGET_HELM_DIR)/$(HELM_ARTIFACT) $(DEPLOY_HELM_DIR)/$(HELM_ARTIFACT) --acl  bucket-owner-full-control);

helm-single:
	@ res=$$(kubectl label namespace $(NAMESPACE) istio-injection=enabled --overwrite); \
	if [[ "$(SYSTEM_API_ACCESS_KEY)" == "" ]]; then \
		echo "You need to set a SYSTEM_API_ACCESS_KEY in your environment. see https://bzdev.bluzone.io/docs/gettingstarted/installation/"; \
		exit 1; \
	fi; \
	VALUES_FILE=values.yaml; \
	if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
		if [ -f $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/userlocal_values.yaml ]; then \
		VALUES_FILE=userlocal_values.yaml; \
		echo "-- running in minikube and found a userlocal_values.yaml present. we will use for values"; \
		fi; \
	fi; \
	if [ -f $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE ]; then \
		if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
			echo "-- setting CLOUD_HOST: $(CLOUD_HOST)"; \
			sed -i.bak "/CLOUD_HOST:/ s/\".*\"/\"$(CLOUD_HOST)\"/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE; \
           	rm -Rf $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE.bak; \
    	fi; \
		echo ""; \
		echo "-- Deploying using branch values from $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE"; \
		echo ""; \
		helm upgrade --namespace=$(NAMESPACE) -i $(HELM_RELEASE_NAME) $(DEPLOY_HELM_DIR)/$(HELM_ARTIFACT) \
			--values=$(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/$$VALUES_FILE \
			--set global.isAmazon="$(IS_AMAZON)" \
			--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
			--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
			--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY) \
			--set global.hosts={$(CLUSTER_HOSTNAME)}; \
	elif [ -f $(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml ]; then \
		echo ""; \
		echo "-- Deploying using hostname values from:  $(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml"; \
		echo ""; \
		helm upgrade --namespace=$(NAMESPACE) -i $(HELM_RELEASE_NAME) $(DEPLOY_HELM_DIR)/$(HELM_ARTIFACT) --values=$(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml \
			--set global.isAmazon="$(IS_AMAZON)" \
			--set global.hosts={$(CLUSTER_HOSTNAME)} \
			--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
			--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
			--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY); \
	else \
		if [ -f $(HELM_ENV_DIR)/values.yaml ]; then \
			echo ""; \
			echo "-- Deploying using root values from $(HELM_ENV_DIR)/values.yaml"; \
			echo ""; \
			helm upgrade --namespace=$(NAMESPACE) -i $(HELM_RELEASE_NAME) $(DEPLOY_HELM_DIR)/$(HELM_ARTIFACT) \
				--set global.isAmazon="$(IS_AMAZON)" \
				--values=$(HELM_ENV_DIR)/values.yaml \
				--set global.hosts={$(CLUSTER_HOSTNAME)} \
				--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
				--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
				--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY); \
		else \
			echo ""; \
			echo "-- Deploying using no values files as $(HELM_ENV_DIR)/values.yaml was no present."; \
			echo ""; \
			helm upgrade --namespace=$(NAMESPACE) -i $(HELM_RELEASE_NAME) $(DEPLOY_HELM_DIR)/$(HELM_ARTIFACT) \
				--set global.isAmazon="$(IS_AMAZON)" \
				--set global.hosts={$(CLUSTER_HOSTNAME)}  \
				--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
				--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
				--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY); \
		fi; \
	fi; \

# deploy this helm using helm. Remote copy will be retrieved
# Search Order
# CONFIG=bob /env/bob/values.yaml
# env/{branch}-{domain}/values.yaml
# env/{domain}/values.yaml
# env/values.yaml
#
# minikube special case:
# userlocal_values.yaml (ignored file that each dev can update)
# values.yaml (bare minimum example)
helm-deploy: helm-add kube-set-context init-namespace route53-host copy-helm helm-single


#########################
# end: helm
#########################

# bounce-reg-creds can be used in minikube to update the credentials
bounce-reg-creds:
	@ if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
		kubectl -n kube-system delete pods $$(kubectl -n kube-system get pods -l name=registry-creds -o=jsonpath='{.items[0].metadata.name}'); \
	fi;

# TODO: This should deprecate once our release process is nailed down.
# alias to bump-version
bv: bump-version
# bump-version can be used to increment the patch version of the git repo.
# 	semver expected is 0.0.0 will move to 0.0.1 when calling this.
# to increment major or minor one should tag the branch
bump-version:
	git tag -a $(NEXT_HOTFIX_VERSION) -m 'pipe-auto: version increase'
	git push --tags; \
	echo "-- version bump from: $(CURRENT_RELEASE) to $(NEXT_HOTFIX_VERSION)";



define RELEASE_DOC
+++
title= "Release $(NEXT_RELEASE_VERSION)"
+++

# Release $(NEXT_RELEASE_VERSION)

## Highlights

Release $(NEXT_RELEASE_VERSION) is scheduled release for bug fixes and enhancements.

## Known Issues

There are no known issues at this time.

##### Summary of Changes

endef

export RELEASE_DOC


# Create And Finish Release is convenience to cycle through release process
caf-release: new-release finish-release

# checkout the next release branch version
new-release: next-release
next-release:
	git checkout -b $(NEXT_RELEASE_BRANCH) master
	mkdir -p $(ROOT_DOCS)
	echo "$$RELEASE_DOC" > $(ROOT_DOCS)/$(NEXT_RELEASE_VERSION).md
	git add $(ROOT_DOCS)/$(NEXT_RELEASE_VERSION).md
	git commit -m 'pipe-auto: preparing release notes ' $(ROOT_DOCS)/$(NEXT_RELEASE_VERSION).md
	git push --set-upstream origin $(NEXT_RELEASE_BRANCH)


# checkout release branch
checkout-release:
	git checkout $(NEXT_RELEASE_BRANCH)

# finish a release branch
finish-release: checkout-release git-check-dirty git-remote-equal prepare-release-notes
	git tag -a $(NEXT_RELEASE_VERSION) -m 'pipe-auto: version increase'
	git checkout master
	git merge $(NEXT_RELEASE_BRANCH)
	git push --tags origin master
	git branch -d $(NEXT_RELEASE_BRANCH)
#	When preventing direct merge to master we can use pull request to signify we will deploy to prod.
	open "https://$(PACKAGE_ROOT)/pull-requests/new?source=$(NEXT_RELEASE_BRANCH)&dest=$(MARKER_BRANCH_NAME)"
#	git checkout $(MARKER_BRANCH_NAME)
#	git merge --ff-only $(NEXT_RELEASE_VERSION)



########## HOTFIX ##############

define HF_DOC
+++
title= "Release $(NEXT_HOTFIX_VERSION)"
+++

# Release $(NEXT_HOTFIX_VERSION)

## Highlights

Release $(NEXT_HOTFIX_VERSION) is scheduled release for bug fixes and enhancements.

## Known Issues

There are no known issues at this time.

##### Summary of Changes

endef

export HF_DOC
# Create And Finish Release is convenience to cycle through hotfix process
hf-release: new-hotfix finish-hotfix

# checkout the next hotfix branch version
new-hotfix: next-hotfix
next-hotfix:
	git checkout -b $(NEXT_HOTFIX_BRANCH) master
	mkdir -p $(ROOT_DOCS)
	echo "$$HF_DOC" > $(ROOT_DOCS)/$(NEXT_HOTFIX_VERSION).md
	git add $(ROOT_DOCS)/$(NEXT_HOTFIX_VERSION).md
	git commit -m 'pipe-auto: preparing hotfix notes' $(ROOT_DOCS)/$(NEXT_HOTFIX_VERSION).md
	git push --set-upstream origin $(NEXT_HOTFIX_BRANCH)


# checkout hotifx branch
checkout-hotfix:
	git checkout $(NEXT_HOTFIX_BRANCH)

# finish a release branch
finish-hotfix: checkout-hotfix git-check-dirty git-hf-equal prepare-hotfix-notes
	git tag -a $(NEXT_HOTFIX_VERSION) -m 'pipe-auto: version increase'
	git checkout master
	git merge $(NEXT_HOTFIX_BRANCH)
	git push --tags origin master
	git branch -d $(NEXT_HOTFIX_BRANCH)
#	When preventing direct merge to master we can use pull request to signify we will deploy to prod.
	open "https://$(PACKAGE_ROOT)/pull-requests/new?source=$(NEXT_HOTFIX_BRANCH)&dest=$(MARKER_BRANCH_NAME)"
#	git checkout $(MARKER_BRANCH_NAME)
#	git merge --ff-only $(NEXT_RELEASE_VERSION)



# check if we have changed to commit in branch
git-check-dirty:
	@ isdirty=$$(git status -s | wc -l | xargs); \
	if [[ $$isdirty == "0" ]]; then \
		echo "-- No changes to commit"; \
	else \
		echo "-- ERROR - You have changes to commit. Exiting now."; \
		exit 1; \
	fi;

# check that our local / equals remote and we don't need to git pull changes
git-hf-equal:
	@ isdirty=$$(git diff $(NEXT_HOTFIX_BRANCH) origin/$(NEXT_HOTFIX_BRANCH) --name-only | wc -l | xargs); \
	if [[ $$isdirty == "0" ]]; then \
		echo "-- Local branch $(NEXT_HOTFIX_BRANCH) equals remote branch origin/$(NEXT_HOTFIX_BRANCH)"; \
	else \
		echo "-- ERROR - Changes exist and local $(NEXT_HOTFIX_BRANCH) does not equal remote origin/$(NEXT_HOTFIX_BRANCH). Exiting now."; \
		exit 1; \
	fi;

# check that our local / equals remote and we don't need to git pull changes
git-remote-equal:
	@ isdirty=$$(git diff $(NEXT_RELEASE_BRANCH) origin/$(NEXT_RELEASE_BRANCH) --name-only | wc -l | xargs); \
	if [[ $$isdirty == "0" ]]; then \
		echo "-- Local branch $(NEXT_RELEASE_BRANCH) equals remote branch origin/$(NEXT_RELEASE_BRANCH)"; \
	else \
		echo "-- ERROR - Changes exist and local $(NEXT_RELEASE_BRANCH) does not equal remote origin/$(NEXT_RELEASE_BRANCH). Exiting now."; \
		exit 1; \
	fi;

# Convenience to Open the new pull request dialog for the current branch
pr:
	open https://bitbucket.org/bluvision-cloud/$(REPO_NAME)/pull-requests/new?source=$(GIT_BRANCH)

# alias to up the bom version
ubv: up-bom-version

# This will trigger the branch deploy of the bom
# If we trigger via a pipeline
#	curl -X POST -is -u $(BB_LOGIN):$(BB_PASSWORD)  -H 'Content-Type: application/json' https://api.bitbucket.org/2.0/repositories/bluvision-cloud/bom/pipelines/ \
#      -d ' {"target": {"ref_type": "branch","type": "pipeline_ref_target","ref_name": "$(GIT_BRANCH)" }}'
# YOU CAN SKIP bom update by adding 'skipbom' to your commit
up-bom-version:
	@ echo "-- bom branch [$(GIT_BRANCH)] set version: $(HELM_BASE)@$(IMAGE_TAG)"; \
	commitMessage=$$(git log --format=%B -n 1 $(GIT_COMMIT) | xargs); \
	case "$$commitMessage" in \
		*"skipbom"* | *"skip bom"* ) \
			echo "skipbom found.. will not update bom.. orig message: $$commitMessage"; \
			exit 0; \
		;; \
		*) \
			echo "-- trigger bom update"; \
		;; \
	esac; \
	rm -Rf $(BOM_OUT_DIR)/$(BOM_REPO_SLUG); \
	GRES=$$(git clone --depth 1 --quiet --branch $(GIT_BRANCH) --single-branch $(BOM_REPO) $(BOM_OUT_DIR)/$(BOM_REPO_SLUG) 2>&1); \
	if [[ "$$GRES" != "" ]]; then \
		echo "-- ERROR $$GRES"; \
		exit 1; \
	fi; \
	mdir=$$(pwd); \
	cd  $(BOM_OUT_DIR)/$(BOM_REPO_SLUG); \
	git config user.name "pipelineuser"; \
	git config  user.email "clouddev@bluvision.com"; \
	git commit -m "pipe-version: $(HELM_BASE)@$(IMAGE_TAG)" --quiet --allow-empty; \
	git branch -u origin/$(GIT_BRANCH) --quiet; \
	val=$$(git push --quiet 2>&1); \
	cd $$mdir; \
	rm -Rf $(BOM_OUT_DIR)/$(BOM_REPO_SLUG)

# This will grab all commits between two releases and output the 'Done' JIRA issues to the changes summary.
# To output the issue key BCP-2222; change below to add  $$key before $$summary
# Changes will be written to the projects documentation by default in resources/docs/content/releases/{myrelase:v1.0.0}.md
prepare-hotfix-notes:
	@ echo "preparing release notes from $(CURRENT_RELEASE) $(CURRENT_RELEASE_COMMIT) to $(NEXT_HOTFIX_VERSION) $(NEXT_RELEASE_COMMIT)"; \
	if [[ $(BB_LOGIN) == "notvalid" ]]; then \
		echo "-- ERROR - BB_LOGIN was not defined. Please add to environment before running release. Exiting now."; \
		exit 1; \
	fi; \
	if [[ $(BB_PASSWORD) == "notvalid" ]]; then \
		echo "-- ERROR - BB_PASSWORD was not defined. Please add to environment before running release. Exiting now."; \
		exit 1; \
	fi; \
	T_COMMITS=$$(curl -XGET -s -u "$(BB_LOGIN):$(BB_PASSWORD)"  $(HISTORY_URL) | jq -r '.values[] | select(.message | contains("'$(JIRA_TAG)'")) | .message'); \
	JIRA_COMMITS=""; \
	for j in $$T_COMMITS; do \
	  if [[ $$j == *"$(JIRA_TAG)"* ]]; then \
	  	JIRA_COMMITS+="$$j \n"; \
	  fi; \
    done; \
	GOOD_COMMITS=$$(echo $$JIRA_COMMITS | uniq); \
	COUNT_CMT=$$(echo $$GOOD_COMMITS | wc -l | xargs); \
	echo "-- Found $$COUNT_CMT jira tagged commits to consider for release notes"; \
	for key in $$GOOD_COMMITS; do \
		issue=$$(curl -XGET -s -H'Accept: application/json' -u "$(BB_LOGIN):$(BB_PASSWORD)" https://bluvision.atlassian.net/rest/api/2/issue/$$key); \
		status=$$(echo $$issue | jq -r '.fields["status"].name'); \
		summary=$$(echo $$issue | jq -r '.fields["summary"]'); \
		if [[ $$status == *"Done"* ]]; then \
			echo "-- Adding $$key / status: $$status to release notes"; \
			echo "* $$summary" >> $(ROOT_DOCS)/$(NEXT_HOTFIX_VERSION).md; \
		else \
			echo "-- Not Adding commit $$key with status: $$status as not Done."; \
		fi; \
	done; \
	echo "   " >> $(ROOT_DOCS)/$(NEXT_HOTFIX_VERSION).md; \
	git commit -m 'pipe-auto: released hotfix $(NEXT_HOTFIX_VERSION) updated release notes' $(ROOT_DOCS)/$(NEXT_HOTFIX_VERSION).md;\
	git push;



# This will grab all commits between two releases and output the 'Done' JIRA issues to the changes summary.
# To output the issue key BCP-2222; change below to add  $$key before $$summary
# Changes will be written to the projects documentation by default in resources/docs/content/releases/{myrelase:v1.0.0}.md
prepare-release-notes:
	@ echo "preparing release notes from $(CURRENT_RELEASE) $(CURRENT_RELEASE_COMMIT) to $(NEXT_RELEASE_VERSION) $(NEXT_RELEASE_COMMIT)"; \
	if [[ $(BB_LOGIN) == "notvalid" ]]; then \
		echo "-- ERROR - BB_LOGIN was not defined. Please add to environment before running release. Exiting now."; \
		exit 1; \
	fi; \
	if [[ $(BB_PASSWORD) == "notvalid" ]]; then \
		echo "-- ERROR - BB_PASSWORD was not defined. Please add to environment before running release. Exiting now."; \
		exit 1; \
	fi; \
	T_COMMITS=$$(curl -XGET -s -u "$(BB_LOGIN):$(BB_PASSWORD)"  $(HISTORY_URL) | jq -r '.values[] | select(.message | contains("'$(JIRA_TAG)'")) | .message'); \
	JIRA_COMMITS=""; \
	for j in $$T_COMMITS; do \
	  if [[ $$j == *"$(JIRA_TAG)"* ]]; then \
	  	JIRA_COMMITS+="$$j \n"; \
	  fi; \
    done; \
	GOOD_COMMITS=$$(echo $$JIRA_COMMITS | uniq); \
	COUNT_CMT=$$(echo $$GOOD_COMMITS | wc -l | xargs); \
	echo "-- Found $$COUNT_CMT jira tagged commits to consider for release notes"; \
	for key in $$GOOD_COMMITS; do \
		issue=$$(curl -XGET -s -H'Accept: application/json' -u "$(BB_LOGIN):$(BB_PASSWORD)" https://bluvision.atlassian.net/rest/api/2/issue/$$key); \
		status=$$(echo $$issue | jq -r '.fields["status"].name'); \
		summary=$$(echo $$issue | jq -r '.fields["summary"]'); \
		if [[ $$status == *"Done"* ]]; then \
			echo "-- Adding $$key / status: $$status to release notes"; \
			echo "* $$summary" >> $(ROOT_DOCS)/$(NEXT_RELEASE_VERSION).md; \
		else \
			echo "-- Not Adding commit $$key with status: $$status as not Done."; \
		fi; \
	done; \
	echo "   " >> $(ROOT_DOCS)/$(NEXT_RELEASE_VERSION).md; \
	git commit -m 'pipe-auto: released $(NEXT_RELEASE_VERSION) updated release notes' $(ROOT_DOCS)/$(NEXT_RELEASE_VERSION).md;\
	git push;


# clean will remove all generated artifacts
clean:
	rm -Rf $(BOM_OUT_DIR)/$(BOM_REPO_SLUG)
	rm -Rf $(BOM_HELM_DIR)/$(BOM_BASE)/charts/*
	rm -Rf $(BOM_HELM_DIR)/*.tgz
	rm -Rf $(BOM_HELM_DIR)/index.yaml
	rm -Rf $(BOM_HELM_DIR)/update.yaml
	rm -Rf $(DEPLOY_HELM_DIR)/$(HELM_BASE)/charts/*
	rm -Rf $(DEPLOY_HELM_DIR)/*.tgz
	rm -Rf $(DEPLOY_HELM_DIR)/index.yaml
	rm -Rf $(DEPLOY_HELM_DIR)/update.yaml

# build tag and push docker image
docker : docker-build docker-tag docker-push

# Builds inside ci container using multistage docker
docker-c%:
	@$(shell aws ecr get-login  --region=$(ECR_REGION) --registry-id=$(ECR_REGISTRY_ID) --no-include-email)
	docker build -f Dockerfile.multi --build-arg VERSION=$(SEM_VER) -t $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG) $(DOCKER_CI_CTX)

docker-buil%:
	docker build -t $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG) .

docker-ta%:
	@ if [ -z $$TAG ]; then \
		echo "-- TAG has not been explicity set. using $(IMAGE_SHORT_TAG)"; \
		docker tag $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG) $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_SHORT_TAG); \
		docker tag $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG) $(ECR_REGISTRY)/$(REPO_NAME):$(LEGACY_TAG); \
		docker tag $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG) $(ECR_REGISTRY)/$(ADDITIONAL_REPO_NAME):$(IMAGE_SHORT_TAG); \
		docker tag $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG) $(ECR_REGISTRY)/$(ADDITIONAL_REPO_NAME):$(LEGACY_TAG); \
	fi;
	@ if [[ $(GIT_BRANCH) = "$(MARKER_BRANCH_NAME)" ]]; then \
		echo "-- Running on marker branch: $(MARKER_BRANCH_NAME). Tagging with latest"; \
		docker tag $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG) $(ECR_REGISTRY)/$(REPO_NAME):latest; \
	fi; \

docker-pus%: force-repo
	@$(shell aws ecr get-login  --region=$(ECR_REGION) --registry-id=$(ECR_REGISTRY_ID) --no-include-email)
	docker push $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG)
	@ if [ -z $$TAG ]; then \
		echo "No tag"; \
		docker push $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_SHORT_TAG); \
		docker push $(ECR_REGISTRY)/$(REPO_NAME):$(LEGACY_TAG); \
		docker push $(ECR_REGISTRY)/$(ADDITIONAL_REPO_NAME):$(IMAGE_SHORT_TAG); \
		docker push $(ECR_REGISTRY)/$(ADDITIONAL_REPO_NAME):$(LEGACY_TAG); \
	fi;
	@ if [[ $(GIT_BRANCH) = "$(MARKER_BRANCH_NAME)" ]]; then \
		echo "Running on marker branch $(MARKER_BRANCH_NAME). Tagging with latest"; \
		docker push $(ECR_REGISTRY)/$(REPO_NAME):latest; \
	fi; \

# fore-repo will make sure we have  a repo in the root account that we can use and set the proper policy when creating
force-rep%:
	@ current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
	case "$$current_acct" in \
		*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
		*) \
		credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
		export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
		export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
		export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
		echo "-- assumed root access"; \
		;; \
	esac; \
	RES=$$(aws ecr describe-repositories --region=$(ECR_REGION) --registry-id=$(ECR_REGISTRY_ID) --repository-names=$(REPO_NAME) 2>&1 ); \
	case "$$RES" in \
		*createdAt*) echo "-- repository exists. nothing more to do";;\
		*) \
		echo "-- repo $(REPO_NAME) does not exist. creating a new one..."; \
		CREATE_RES=$$(aws ecr create-repository --region=$(ECR_REGION)  --repository-name=$(REPO_NAME)); \
		echo "-- Creating policy for new repo"; \
        mkdir -p $(BOM_OUT_DIR); \
        statement="{\"Version\": \"2008-10-17\",\"Statement\": [{ \"Sid\": \"Organizational Units\",\"Effect\": \"Allow\",\"Principal\": \"*\",\"Condition\": {\"StringEquals\": {\"aws:PrincipalOrgID\": \"$(ORG_ID)\"}},\"Action\": [\"ecr:*\" ]}]}"; \
        echo $$statement > $(BOM_OUT_DIR)/temp_policy.json; \
        aws ecr set-repository-policy --registry-id=$(ECR_REGISTRY_ID) --repository-name=$(REPO_NAME) --policy-text=file://$(BOM_OUT_DIR)/temp_policy.json; \
        rm -Rf $(BOM_OUT_DIR)/temp_policy.json; \
		;; \
	esac; \


# inf target is convenient to output what variables what be run with when executing
inf%:
	@echo "------ Deployment Overview -------"
	@echo ""
	@echo "deploy into AWS account [$(B_PROFILE)] using eks cluster [$(EKS_CLUSTERNAME)] / namespace [$(NAMESPACE)] available at [https://$(CLUSTER_HOSTNAME)]."
	@echo ""
	@echo "------ Branch Values ------"
	@echo GIT_BRANCH: $(GIT_BRANCH)
	@echo CLEAN_BRANCH: $(CLEAN_BRANCH)
	@echo B_PROCEDURE: $(B_PROCEDURE)
	@echo B_PROFILE: $(B_PROFILE)
	@echo B_KUBECONTEXT: $(B_KUBECONTEXT)
	@echo NAMESPACE - K8s Namespace: $(NAMESPACE)
	@echo ""
	@echo "------ Versioning ------"
	@echo ""
	@echo MAJOR: $(MAJOR)
	@echo MINOR: $(MINOR)
	@echo PATCH: $(PATCH)
	@echo MAJOR_MINOR: $(MAJOR).$(MINOR)
	@echo Sem Ver: $(SEM_VER)
	@echo CURRENT_RELEASE: $(CURRENT_RELEASE)
	@echo CURRENT_RELEASE_COMMIT: $(CURRENT_RELEASE_COMMIT)
	@echo NEXT_RELEASE_VERSION: $(NEXT_RELEASE_VERSION)
	@echo NEXT_RELEASE_COMMIT: $(NEXT_RELEASE_COMMIT)
	@echo ""
	@echo REGION: $(REGION)
	@echo ENVIRONMENT_BUCKET_NAME: $(ENVIRONMENT_BUCKET_NAME)
	@echo CALLER_ID: $(CALLER_ID)
	@echo EKS_CLUSTERNAME: $(EKS_CLUSTERNAME)
	@echo CLUSTER_NAME: $(CLUSTER_NAME)
	@echo CLUSTER_HOSTNAME: $(CLUSTER_HOSTNAME)
	@echo CLOUD_HOST: $(CLOUD_HOST)
	@echo BRANCH_HELM_REPO: $(BRANCH_HELM_REPO)
	@echo BRANCH_HELM_REPO_URL: $(BRANCH_HELM_REPO_URL)
	@echo EKS_CLUSTERNAME: $(EKS_CLUSTERNAME)
	@echo REPO_NAME: $(REPO_NAME)
	@echo ENV_HOSTNAME: $(ENV_HOSTNAME)
	@echo CLEAN_ENV_HOSTNAME: $(CLEAN_ENV_HOSTNAME)
	@echo CLEAN_HOSTNAME: $(CLEAN_HOSTNAME)
	@echo Package root: $(PACKAGE_ROOT)
	@echo Docker: $(ECR_REGISTRY)/$(REPO_NAME):$(IMAGE_TAG)
	@echo Build Number: $(BUILD_NUMBER)
	@echo GIT_TAG: $(GIT_TAG)
	@echo IMAGE_SHORT_TAG: $(IMAGE_SHORT_TAG)
	@echo IMAGE_TAG: $(IMAGE_TAG)
	@echo LD_FLAGS: $(LD_FLAGS)
	@echo SRC_DIR: $(SRC_DIR)
	@echo ABSOLUTE_SRC_DIR: $(ABSOLUTE_SRC_DIR)
	@echo ROOT_DEPLOY: $(ROOT_DEPLOY)
	@echo DOCKER_CI_CTX: $(DOCKER_CI_CTX)
	@echo TARGET_HELM_REPO: $(TARGET_HELM_REPO)
	@echo DEPENDS_HELM_REPO: $(DEPENDS_HELM_REPO)
	@echo BOM_RELEASE_NAME: $(BOM_RELEASE_NAME)
	@echo BOM_ARTIFACT: $(BOM_ARTIFACT)
	@echo NAMESPACE: $(NAMESPACE)
	@echo TARGET_HELM_CONFIG: $(TARGET_HELM_CONFIG)
	@echo AWS_ACCESS_KEY_ID: $(AWS_ACCESS_KEY_ID)
	@echo GATEWAY_VERSION: $(GATEWAY_VERSION)

define README_DOC
+++
title = "$(REPO_NAME) documentation"
description = ""
+++

# $(REPO_NAME) documentation

endef

export README_DOC
define DOC_INIT
baseURL = "http://example.org/"
languageCode = "en-us"
title="$(REPO_NAME)"
theme="dot"
endef
export DOC_INIT
initdocs:
	@ mkdir -p resources
	@ cd resources && hugo new site docs
	@ cd resources/docs && git clone https://github.com/themefisher/dot-hugo-documentation-theme themes/dot
	@ rm -Rf resources/docs/themes/dot/.git
	@ echo "$$DOC_INIT" > resources/docs/config.toml
	@ echo "$$README_DOC" > resources/docs/content/_index.md


# install the dependencies
depends-ini%:
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go get -u github.com/golang/protobuf/protoc-gen-go
	go get -u google.golang.org/grpc
	go get -u github.com/gogo/protobuf/protoc-gen-gogoslick
	go get -u github.com/golang/dep/cmd/dep

resources:  proto-gen gateway-gen openapi-gen

proto-ge%:
	rm -Rf $(ABSOLUTE_SRC_DIR)/$(PROTO_PKG)/*.pb.go
	cd $(ABSOLUTE_SRC_DIR)/$(PROTO_PKG); \
	protoc $(PROTO_INCLUDE) \
	--$(PROTOC_EXTENSION)=Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,plugins=grpc:./ *.proto

gateway-ge%:
	cd $(ABSOLUTE_SRC_DIR)/$(PROTO_PKG); \
	protoc $(PROTO_INCLUDE) \
	 --grpc-gateway_out=logtostderr=true,allow_delete_body=true:./ *.proto.gw

openapi-ge%:
	cd $(ABSOLUTE_SRC_DIR)/$(PROTO_PKG); \
	protoc $(PROTO_INCLUDE) \
	 --swagger_out=logtostderr=true,allow_delete_body=true:$(ABSOLUTE_SRC_DIR)/resources/apis/openapi *.proto.gw
	 python $(ABSOLUTE_SRC_DIR)/resources/scripts/clean_openapi.py -f $(ABSOLUTE_SRC_DIR)/resources/apis/openapi/$(SWAGGER_OUT).proto.swagger.json -e $(ABSOLUTE_SRC_DIR)/resources/apis/openapi/$(SWAGGER_OUT).proto.swagger.json

upkit:
	go get bitbucket.org/bluvision-cloud/kit

fix-minikube-time:
	minikube ssh -- sudo date -u $(date -u +%m%d%H%M%Y.%S)


# To generate docs you should provide a value for DOC_PKGS that list your packages to generate docs for
# Example: DOC_PKGS:=config
doc-ge%:
	$(foreach pkg,$(DOC_PKGS),godoc2md $(PACKAGE_ROOT)/$(pkg) > $(GOPATH)/src/$(PACKAGE_ROOT)/$(pkg)/README.md;)

kube-set-context:
	@ echo "";
	@ if [[ $(CLUSTER_NAME) = "minikube" ]]; then \
		kubectl config use-context minikube; \
	else \
		aws eks update-kubeconfig --name=$(EKS_CLUSTERNAME); \
	fi;
	@ echo "";

# Convenience for all our bz-infra deployers
deploy-all: deploy-bzdev deploy-bztest deploy-bzprod

deploy-bzdev:
	git checkout bzdev && git merge master && git push && git checkout master
deploy-bztest:
	git checkout bztest && git merge master && git push && git checkout master
deploy-bzprod:
	git checkout bzprod && git merge master && git push && git checkout master


# make sure we have a repo for our master branch
ensure-master-repo:
	@ while true;\
    	do \
    		 if [[ $(CLEAN_BRANCH) = "master" ]]; then \
    		 		echo "-- nothing to ensure for master as we are master"; \
            		exit; \
			fi; \
    		current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
    		case "$$current_acct" in \
    			*$(ECR_REGISTRY_ID)*) ;; \
    			*) \
    			credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "RepoInit"); \
    			export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
    			export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
    			export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
    			echo "-- assumed root access"; \
    			;; \
    		esac; \
    		res=$$(aws s3api get-object --bucket=$(HELM_BUCKET) --key=$(MASTER_HELM_REPO)/update.yaml $(DEPLOY_HELM_DIR)/update.yaml 2>&1); \
			case "$$res" in \
            	*NoSuchKey*) \
            		echo "-- We need to initialize chart repo at $(MASTER_HELM_REPO_URL)"; \
            		helm repo index $(DEPLOY_HELM_DIR); \
					ETAG=$$(aws s3api put-object --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(MASTER_HELM_REPO)/update.yaml --body $(DEPLOY_HELM_DIR)/index.yaml | jq '.ETag'); \
					ETAG=$${ETAG:3:32}; \
					CPRES=$$(aws s3api copy-object --copy-source=$(HELM_BUCKET)/$(MASTER_HELM_REPO)/update.yaml --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(MASTER_HELM_REPO)/index.yaml --copy-source-if-match=$$ETAG 2>&1); \
					case "$$CPRES" in \
						*PreconditionFailed*) \
							echo "-- Merge attempt failed because update.yml was inconsistent. Trying again."; \
						;;\
						*) \
							echo "-- Successfully initialized chart repo for $(MASTER_HELM_REPO_URL)"; \
							break; \
						;; \
					esac; \
            	;;\
            	*) \
            		break; \
            	;;\
            esac; \
    	done

# make sure we have a repo for our master branch
ensure-release-repo:
	@ while true;\
    	do \
    		 if [[ $(CLEAN_BRANCH) = "bzprod" ]]; then \
    		 		echo "-- nothing to ensure for bzprod as we are bzprod"; \
            		exit; \
			fi; \
    		current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
    		case "$$current_acct" in \
    			*$(ECR_REGISTRY_ID)*)  ;; \
    			*) \
    			credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "RepoInit"); \
    			export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
    			export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
    			export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
    			echo "-- assumed root access"; \
    			;; \
    		esac; \
    		res=$$(aws s3api get-object --bucket=$(HELM_BUCKET) --key=$(RELEASE_HELM_REPO)/update.yaml $(DEPLOY_HELM_DIR)/update.yaml 2>&1); \
			case "$$res" in \
            	*NoSuchKey*) \
            		echo "-- We need to initialize chart repo at $(RELEASE_HELM_REPO_URL)"; \
            		helm repo index $(DEPLOY_HELM_DIR); \
					ETAG=$$(aws s3api put-object --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(RELEASE_HELM_REPO)/update.yaml --body $(DEPLOY_HELM_DIR)/index.yaml | jq '.ETag'); \
					ETAG=$${ETAG:3:32}; \
					CPRES=$$(aws s3api copy-object --copy-source=$(HELM_BUCKET)/$(RELEASE_HELM_REPO)/update.yaml --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(RELEASE_HELM_REPO)/index.yaml --copy-source-if-match=$$ETAG 2>&1); \
					case "$$CPRES" in \
						*PreconditionFailed*) \
							echo "-- Merge attempt failed because update.yml was inconsistent. Trying again."; \
						;;\
						*) \
							echo "-- Successfully initialized chart repo for $(RELEASE_HELM_REPO_URL)"; \
							break; \
						;; \
					esac; \
            	;;\
            	*) \
            		break; \
            	;;\
            esac; \
    	done



# make sure we have a repo for our helm charts for branch
ensure-branch-repo:
	@ while true;\
    	do \
    		current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
    		case "$$current_acct" in \
    			*$(ECR_REGISTRY_ID)*) ;; \
    			*) \
    			credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "RepoInit"); \
    			export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
    			export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
    			export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
    			echo "-- assumed root access"; \
    			;; \
    		esac; \
    		res=$$(aws s3api get-object --bucket=$(HELM_BUCKET) --key=$(BRANCH_HELM_REPO)/update.yaml $(DEPLOY_HELM_DIR)/update.yaml 2>&1); \
			case "$$res" in \
            	*NoSuchKey*) \
            		echo "-- We need to initialize chart repo at $(BRANCH_HELM_REPO_URL)"; \
            		helm repo index $(DEPLOY_HELM_DIR); \
					ETAG=$$(aws s3api put-object --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(BRANCH_HELM_REPO)/update.yaml --body $(DEPLOY_HELM_DIR)/index.yaml | jq '.ETag'); \
					ETAG=$${ETAG:3:32}; \
					CPRES=$$(aws s3api copy-object --copy-source=$(HELM_BUCKET)/$(BRANCH_HELM_REPO)/update.yaml --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(BRANCH_HELM_REPO)/index.yaml --copy-source-if-match=$$ETAG 2>&1); \
					case "$$CPRES" in \
						*PreconditionFailed*) \
							echo "-- Merge attempt failed because update.yml was inconsistent. Trying again."; \
						;;\
						*) \
							echo "-- Successfully initialized chart repo for $(BRANCH_HELM_REPO_URL)"; \
							break; \
						;; \
					esac; \
            	;;\
            	*) \
            		break; \
            	;;\
            esac; \
    	done

############################################################################
# cloudformation - integration for optional cf resourcess
############################################################################

# we will render a cloudformation parameter file if we find a template file and we do not already have a parameters file rendered.
cf-render-params:
	@ if [ -f $(DEPLOY_AWS_DIR)/master-parameters.tmpl.yaml ]; then \
			if [ -f $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.yaml ]; then \
				echo "-- namespace formation parameters already exist at $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.yaml. not rendering new one"; \
			else \
				echo "-- rendering new namespace formation parameters file into $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.yaml"; \
				mkdir -p $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG); \
				cp $(DEPLOY_AWS_DIR)/master-parameters.tmpl.yaml $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.ClusterName}}/$(EKS_CLUSTERNAME)/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.Namespace}}/$(NAMESPACE)/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.EnvironmentName}}/$(B_KUBECONTEXT)/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.Bucket}}/$(ENVIRONMENT_BUCKET_NAME)/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.StorageRetention}}/$(STORAGE_RETENTION)/g" $(ENV_PARAM_TMPL); \
				NAMESPACE_STACK=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) 2>&1 | xargs); \
				case "{" in \
				*$$NAMESPACE_STACK* ) \
					NAMESPACE_STACK=$$(echo $$NAMESPACE_STACK | jq -r '.Stacks[0].Outputs'); \
					EXIST_PASS=$$(echo $$NAMESPACE_STACK | jq -r '.[] | select(.OutputKey=="DbPassword") | .OutputValue '); \
					sed -i.bak "s/{{.MasterPassword}}/$$EXIST_PASS/g" $(ENV_PARAM_TMPL); \
				;; \
				* ) \
					NEW_PASS=$$(openssl rand -base64 32 | cut -c1-10); \
					sed -i.bak "s/{{.MasterPassword}}/$$NEW_PASS/g" $(ENV_PARAM_TMPL); \
				;; \
				esac; \
				INFRA_STACK=$$(aws cloudformation describe-stacks --stack-name=$(EKS_CLUSTERNAME) | jq -r '.Stacks[0].Outputs'); \
				SubnetA=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="SubnetA") | .OutputValue '); \
				SubnetB=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="SubnetB") | .OutputValue '); \
				SubnetC=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="SubnetC") | .OutputValue '); \
				KeyPairName=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="KeyPairName") | .OutputValue '); \
				NodeInstanceProfile=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="NodeInstanceProfile") | .OutputValue '); \
                NodeInstanceRoleName=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="NodeInstanceRoleName") | .OutputValue '); \
				VPCID=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="VPCID") | .OutputValue '); \
				BASTION_SECURITY_GROUP=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="BastionSecurityGroup") | .OutputValue '); \
				NODE_SECURITY_GROUP_ID=$$(echo $$INFRA_STACK | jq -r '.[] | select(.OutputKey=="NodeSecurityGroupId") | .OutputValue '); \
				sed -i.bak "s/{{.BastionSecurityGroupId}}/$$BASTION_SECURITY_GROUP/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.NodeSecurityGroupId}}/$$NODE_SECURITY_GROUP_ID/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.VPCID}}/$$VPCID/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.SubnetA}}/$$SubnetA/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.SubnetB}}/$$SubnetB/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.SubnetC}}/$$SubnetC/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.KeyPairName}}/$$KeyPairName/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.NodeInstanceProfile}}/$$NodeInstanceProfile/g" $(ENV_PARAM_TMPL); \
				sed -i.bak "s/{{.NodeInstanceRoleName}}/$$NodeInstanceRoleName/g" $(ENV_PARAM_TMPL); \
				cp $(ENV_PARAM_TMPL) $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.yaml; \
				rm -Rf $(ENV_PARAM_TMPL); \
				rm -Rf $(ENV_PARAM_TMPL).bak; \
			fi; \
	else \
		echo "-- namespace formation not required as no formation file found at $(DEPLOY_AWS_DIR)/master-parameters.tmpl.yaml"; \
	fi;

# deploy namespace resources
cf-deploy: cf-render-params
	@ aws s3 cp $(DEPLOY_AWS_DIR)/cloudformation s3://$(ENVIRONMENT_BUCKET_NAME)/$(B_KUBECONTEXT)/$(NAMESPACE)/cloudformation --region=$(REGION) --recursive
	@ cfn-flip $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.yaml $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.json
	@ aws s3 cp $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.json s3://$(ENVIRONMENT_BUCKET_NAME)/$(B_KUBECONTEXT)/$(NAMESPACE)/ --region=$(REGION)
	@ aws s3 cp $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.yaml s3://$(ENVIRONMENT_BUCKET_NAME)/$(B_KUBECONTEXT)/$(NAMESPACE)/ --region=$(REGION)
	@ NAMESPACE_STACK=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) 2>&1 | xargs); \
	case "$$NAMESPACE_STACK" in \
	*"error"* ) \
		echo "-- no namespace formation found for [$(NAMESPACE_CF_NAME)]. we will create new one"; \
		res=$$(aws cloudformation create-stack --stack-name=$(NAMESPACE_CF_NAME) \
			--parameters file://$(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.json \
			--template-url "https://$(ENVIRONMENT_BUCKET_NAME).s3.amazonaws.com/$(B_KUBECONTEXT)/$(NAMESPACE)/cloudformation/master.template.yaml" \
			--capabilities "CAPABILITY_IAM" "CAPABILITY_AUTO_EXPAND" "CAPABILITY_NAMED_IAM"); \
	;; \
	* ) \
		echo "-- namespace formation [$(NAMESPACE_CF_NAME)] exists. we will update"; \
		res=$$(aws cloudformation update-stack --stack-name=$(NAMESPACE_CF_NAME) \
					--parameters file://$(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.json \
					--template-url "https://$(ENVIRONMENT_BUCKET_NAME).s3.amazonaws.com/$(B_KUBECONTEXT)/$(NAMESPACE)/cloudformation/master.template.yaml" \
					--capabilities "CAPABILITY_IAM" "CAPABILITY_AUTO_EXPAND" "CAPABILITY_NAMED_IAM" 2>&1 | xargs); \
		echo "$$res"; \
	;; \
	esac;
	@ CF_STATUS=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) | jq -r '.Stacks[0].StackStatus'); \
	while true;  do\
		case "$$CF_STATUS" in \
		*"COMPLETE"* ) \
			echo "-- namespace formation [$(NAMESPACE_CF_NAME)] finished with status: [$$CF_STATUS]"; \
        	rm -Rf $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/master-parameters.json; \
			break; \
		;; \
		* ) \
			CF_STATUS=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) | jq -r '.Stacks[0].StackStatus'); \
			echo "-- waiting on namespace formation [$(NAMESPACE_CF_NAME)]  to complete. current status: [$$CF_STATUS]"; \
			sleep 30; \
		;; \
		esac; \
	done;

# Ideally you called this via ./bitbucket-release.sh printenv
#AWS_PROFILE=dev_ops EKS_CLUSTERNAME=second-bluzone-io make printenv
#	DBPASS=$$(echo $$STACKOUT | jq -r '.[] | select(.OutputKey=="DbPassword") | .OutputValue '); \
#    ENDPOINT=$$(echo $$STACKOUT | jq -r '.[] | select(.OutputKey=="DatabaseEndpoint") | .OutputValue '); \
#    echo Password: $$DBPASS Endpoint: $$ENDPOINT;
printenv:
	@ STACKOUT=$$(aws cloudformation describe-stacks --stack-name=$(EKS_CLUSTERNAME) | jq -r '.Stacks[0].Outputs'); \
	echo $$STACKOUT | python -m json.tool;
	@ NAMESPACE_STACKOUT=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) | jq -r '.Stacks[0].Outputs'); \
	echo $$NAMESPACE_STACKOUT | python -m json.tool;


# This is extremely conventionalized way to initenv. This is here for example purposes as you most likely
# will want to manually edit the enviontment files.
# use ENAME to set the environment replacement
# create the initial envionrment folder for a new environment from the infra resources created
# AWS_PROFILE=dev_ops EKS_CLUSTERNAME=second-bluzone-io CLUSTER_HOSTNAME=second.bluzone.io ENAME=second make initenv
initenv: cf-deploy
	@ if [ -f $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml ]; then \
		echo "-- initenv found values file at $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml. not creating new one"; \
	else \
		mkdir -p $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG); \
		cp $(HELM_ENV_DIR)/$(STARTER_TEMPLATE) $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		sed -i.bak "s/{{.ZoneDnsName}}/$(ZONE_DNS_NAME)/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		sed -i.bak "s/{{.EnvironmentName}}/$(ENAME)/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		sed -i.bak "s/{{.ClusterWithNamespace}}/$(EKS_CLUSTERNAME)-$(ENAME)/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		sed -i.bak "s/{{.ClusterHostName}}/$(CLUSTER_HOSTNAME)/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		sed -i.bak "s/{{.ClusterName}}/$(EKS_CLUSTERNAME)/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		STACKOUT=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) | jq -r '.Stacks[0].Outputs'); \
		PASS=$$(echo $$STACKOUT | jq -r '.[] | select(.OutputKey=="DbPassword") | .OutputValue '); \
		sed -i.bak "s/{{.DatabasePassword}}/$$PASS/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		ENDPOINT=$$(echo $$STACKOUT | jq -r '.[] | select(.OutputKey=="DatabaseEndpoint") | .OutputValue '); \
		sed -i.bak "s/{{.DatabaseEndpoint}}/$$ENDPOINT/g" $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml; \
		rm -Rf $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml.bak; \
		git pull; \
		git add $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG); \
		git commit -m 'pipe-auto: init-env [skip ci]'; \
		git push --set-upstream origin $(GIT_BRANCH); \
	fi;

# delete the cloudformation resources
cf-delete:
	aws cloudformation delete-stack --stack-name=$(NAMESPACE_CF_NAME)
	@ CF_STATUS=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) | jq -r '.Stacks[0].StackStatus' 2>&1 | xargs); \
	while [[ "$$CF_STATUS" != *"COMPLETE"* ]]; do \
		CF_STATUS=$$(aws cloudformation describe-stacks --stack-name=$(NAMESPACE_CF_NAME) | jq -r '.Stacks[0].StackStatus' 2>&1 | xargs); \
		if [[ "$$CF_STATUS" == *""* ]]; then \
			break; \
		fi; \
		echo "-- waiting on namespace formation [$(NAMESPACE_CF_NAME)]  to delete. current status: [$$CF_STATUS]"; \
		sleep 30; \
	done; \
	echo "-- namespace formation [$(NAMESPACE_CF_NAME)] finished with status: [$$CF_STATUS]";

# purge dynamically provisioned resources... ebs at this time
kube-dynamic-delete: kube-set-context
	@ if [[ "$(IS_PROD)" == "false" ]]; then \
		kubectl delete pvc -n $(NAMESPACE) --all; \
	else \
		echo "-- not allowing volume deelete in a prod account. do so manually"; \
	fi;

# will delete everything in a non prod cluster
delete-env: cf-delete kube-dynamic-delete helm-delete

###########################################################################
# bom (bluzone) - builders for wrapping all of bluzone with this helm chart
###########################################################################

# package and push this bom
bom: helm-add bom-replace bom-package bom-copy-artifact bom-repo-merge

# Replace our pr and dev versions and repos in the reqs file of bom.
bom-replace:
	rm -Rf $(BOM_OUT_DIR)/$(BOM_REPO_SLUG)
	git clone --branch $(BOM_BRANCH) $(BOM_REPO) $(BOM_OUT_DIR)/$(BOM_REPO_SLUG)
	rm -Rf $(BOM_OUT_DIR)/$(BOM_REPO_SLUG)/.git
	sed -i.bak "s/\(<version-$(REPO_NAME)>\)/"\"$(IMAGE_TAG)"\"/g" $(BOM_REQ_PATH)
	sed -i.bak "s/\(<repo-$(REPO_NAME)>\)/"\"\@$(TARGET_HELM_REPO)"\"/g" $(BOM_REQ_PATH)
	sed -i.bak "s/\(<version-.*>\)/"\"\>=0.0.0-0"\"/g" $(BOM_REQ_PATH)
	sed -i.bak "s/\(<repo-.*>\)/"\"\@bluzone-charts-dev"\"/g" $(BOM_REQ_PATH)
	rm -Rf $(BOM_REQ_PATH).bak

# package up the bom
bom-package :
	helm init --client-only
	rm -Rf $(BOM_HELM_DIR)/$(BOM_BASE)/charts/*
	rm -Rf $(BOM_HELM_DIR)/*.tgz
	rm -Rf $(BOM_HELM_DIR)/index.yaml
	rm -Rf $(BOM_HELM_DIR)/update.yaml
	helm dep up $(BOM_HELM_DIR)/$(BOM_BASE)
	helm package --app-version=$(BOM_TAG) --version=$(BOM_TAG) --destination=$(BOM_HELM_DIR) $(BOM_HELM_DIR)/$(BOM_BASE)

# copy bom to s3
bom-copy-artifact:
	@ current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
	case "$$current_acct" in \
		*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
		*) \
		credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
		export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
		export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
		export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
		echo "-- assumed root access"; \
		;; \
	esac; \
	aws s3 cp $(BOM_HELM_DIR)/$(BOM_ARTIFACT)  $(TARGET_HELM_REPO_URL)/$(BOM_ARTIFACT) --acl  bucket-owner-full-control

# merge our new and old chart/index.yaml
bom-repo-merge:
	while true;\
	do \
		 current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
		case "$$current_acct" in \
			*$(ECR_REGISTRY_ID)*) echo "Already root nothing to do.";; \
			*) \
			credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
			export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
			export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
			export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
			echo "-- assumed root access"; \
			;; \
		esac; \
		aws s3api get-object --bucket=$(HELM_BUCKET) --key=$(TARGET_HELM_DIR)/update.yaml $(BOM_HELM_DIR)/update.yaml; \
		helm repo index --merge=$(BOM_HELM_DIR)/update.yaml --url=$(TARGET_HELM_REPO_URL) $(BOM_HELM_DIR); \
		ETAG=$$(aws s3api put-object --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(TARGET_HELM_DIR)/update.yaml --body $(BOM_HELM_DIR)/index.yaml | jq '.ETag'); \
		ETAG=$${ETAG:3:32}; \
		echo $$ETAG; \
		CPRES=$$(aws s3api copy-object --copy-source=$(HELM_BUCKET)/$(TARGET_HELM_DIR)/update.yaml --bucket=$(HELM_BUCKET) --acl  bucket-owner-full-control --key=$(TARGET_HELM_DIR)/index.yaml --copy-source-if-match=$$ETAG 2>&1); \
		echo $$CPRES; \
		case "$$CPRES" in \
			*PreconditionFailed*) \
				echo "Merge attempt failed because update.yml was inconsistent. Trying again."; \
			;;\
			*) \
				echo "Successfully merged our repo"; \
				break; \
			;; \
		esac; \
	done

# delete this helm deployed release and namespace
bom-delete: kube-set-context
	helm delete --purge $(BOM_RELEASE_NAME)
	kubectl delete namespace $(NAMESPACE)

# deploy this bom to cluster
bom-deploy: helm-add kube-set-context init-namespace route53-host
	kubectl label namespace $(NAMESPACE) istio-injection=enabled --overwrite
	aws s3 cp s3://$(HELM_BUCKET)/$(TARGET_HELM_DIR)/$(BOM_ARTIFACT) $(BOM_HELM_DIR)/$(BOM_ARTIFACT) --acl  bucket-owner-full-control; \
	helm upgrade --namespace=$(NAMESPACE) -i $(BOM_RELEASE_NAME) $(BOM_HELM_DIR)/$(BOM_ARTIFACT); \

	@ if [ -f $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml ]; then \
		echo ""; \
		echo "-- Deploying using branch values from $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml"; \
		echo ""; \
		helm upgrade --namespace=$(NAMESPACE) -i $(BOM_RELEASE_NAME) $(BOM_HELM_DIR)/$(BOM_ARTIFACT) \
			--values=$(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml \
			--set global.isAmazon=$(IS_AMAZON) \
			--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
			--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
			--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY) \
			--set global.hosts={$(CLUSTER_HOSTNAME)};
	elif [ -f $(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml ]; then \
		echo ""; \
		echo "-- Deploying using branch values from $(HELM_ENV_DIR)/$(CLEAN_ENV_HOSTNAME)/values.yaml"; \
		echo ""; \
		helm upgrade --namespace=$(NAMESPACE) -i $(BOM_RELEASE_NAME) $(BOM_HELM_DIR)/$(BOM_ARTIFACT) --values=$(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml \
			--set global.isAmazon=$(IS_AMAZON) \
			--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
			--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
			--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY) \
			--set global.hosts={$(CLUSTER_HOSTNAME)};
	else \
		if [ -f $(HELM_ENV_DIR)/values.yaml ]; then \
			echo ""; \
			echo "-- Deploying using root values from $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml"; \
			echo ""; \
			helm upgrade --namespace=$(NAMESPACE) -i $(BOM_RELEASE_NAME) $(BOM_HELM_DIR)/$(BOM_ARTIFACT) --values=$(HELM_ENV_DIR)/values.yaml \
				--set global.isAmazon=$(IS_AMAZON) \
				--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
				--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
				--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY) \
				--set global.hosts={$(CLUSTER_HOSTNAME)};
		else \
			echo ""; \
			echo "-- Deploying using no values files as file: $(HELM_ENV_DIR)/$(TARGET_HELM_CONFIG)/values.yaml was not present."; \
			echo ""; \
			helm upgrade --namespace=$(NAMESPACE) -i $(BOM_RELEASE_NAME) $(BOM_HELM_DIR)/$(BOM_ARTIFACT)
				--set global.isAmazon=$(IS_AMAZON) \
				--set global.secrets.AWS_ACCESS_KEY_ID=$(ORIG_AWS_ACCESS_KEY_ID) \
				--set global.secrets.AWS_SECRET_ACCESS_KEY=$(ORIG_AWS_SECRET_ACCESS_KEY) \
				--set global.secrets.SYSTEM_API_ACCESS_KEY=$(SYSTEM_API_ACCESS_KEY) \
				--set global.hosts={$(CLUSTER_HOSTNAME)};
		fi; \
	fi; \

# check the current version of this file
check-version:
	@ current_acct=$$(aws sts get-caller-identity  | jq -r '.Account'); \
	case "$$current_acct" in \
		*$(ECR_REGISTRY_ID)*) echo "";; \
		*) \
		credentials=$$(aws sts assume-role --role-arn "$(ROOT_ROLE)" --role-session-name "DeleteDns"); \
		export AWS_ACCESS_KEY_ID=$$(echo $$credentials | jq -r '.Credentials.AccessKeyId'); \
		export AWS_SECRET_ACCESS_KEY=$$(echo $$credentials | jq -r '.Credentials.SecretAccessKey'); \
		export AWS_SESSION_TOKEN=$$(echo $$credentials | jq -r '.Credentials.SessionToken'); \
		;; \
	esac; \
	mkversion=$$(cat ~/.bluvision/stable.txt); \
	v=$$(aws s3 cp s3://bluvision-release/goci/stable.txt ~/.bluvision/stable.txt.temp 2>&1); \
	remversion=$$(cat ~/.bluvision/stable.txt.temp); \
	if [[ "$$mkversion" == "$$remversion" ]]; then\
		echo "Current common.mk version is: $$mkversion"; \
	else \
		echo "A New version is available at: $$remversion. Current common.mk version is: $$mkversion."; \
		echo "Run goci/update_common.sh to update to latest version"; \
	fi;
