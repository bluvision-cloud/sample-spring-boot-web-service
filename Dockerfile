FROM adoptopenjdk/openjdk13:jre-13.0.2_8-alpine

MAINTAINER clouddev@bluvision.com
RUN apk update && apk add mysql-client curl

EXPOSE 8080 8083

COPY target/sample-*.jar /data/sandbox.jar
CMD ["java", "-jar", "/data/sandbox.jar"]