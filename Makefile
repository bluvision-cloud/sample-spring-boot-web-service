first: release-artifacts
HELM_BASE:=sandbox
REPO_NAME:=sandbox
PACKAGE_ROOT:=bitbucket.org/bluvision-cloud/sample-spring-boot-web-service
include common.mk
build-java:
	mvn clean install deploy -s bamboo-maven-settings.xml
