package com.bluvision.cloud.web.app;

import com.bluvision.cloud.web.entity.Message;
import com.bluvision.cloud.web.security.config.TestSecurityConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Import(TestSecurityConfig.class)
public class MessageControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void createUser() throws Exception {
        Message message = new Message();
        message.setMessage("welcome");
        MockHttpServletRequestBuilder builder = post("/messages").contentType("application/json").content(mapper.writeValueAsBytes(message));
        MvcResult res = mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andReturn();
        Message respMsg = mapper.readValue(res.getResponse().getContentAsByteArray(), Message.class);
        Assertions.assertNotNull(respMsg.getId());

        mockMvc.perform(get("/messages/" + respMsg.getId())).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("welcome"));
    }
}
