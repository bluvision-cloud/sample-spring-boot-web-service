package com.bluvision.cloud.web.entity;

public enum  AuthProvider {
    local,
    cognito,
    google
}
