package com.bluvision.cloud.web.entity;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
