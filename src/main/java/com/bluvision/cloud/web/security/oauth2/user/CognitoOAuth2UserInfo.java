package com.bluvision.cloud.web.security.oauth2.user;

import java.util.Map;

public class CognitoOAuth2UserInfo extends OAuth2UserInfo {
    public CognitoOAuth2UserInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    @Override
    public String getId() {
        return (String) attributes.get("sub");
    }

    @Override
    public String getUserName() {
        return (String) attributes.get("username");
    }

    @Override
    public String getEmail() {
        return (String) attributes.get("email");
    }

}
