package com.bluvision.cloud.web.controller;

import com.bluvision.cloud.web.entity.RoleName;
import com.bluvision.cloud.web.entity.User;
import com.bluvision.cloud.web.exception.ResourceNotFoundException;
import com.bluvision.cloud.web.repository.UserRepository;
import com.bluvision.cloud.web.security.CurrentUser;
import com.bluvision.cloud.web.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecureController {

    @GetMapping("/user/secure")
    @Secured("ROLE_USER")
    public String userSecure() {
        return RoleName.ROLE_USER.name();
    }

    @GetMapping("/admin/secure")
    @Secured("ROLE_ADMIN")
    public String adminSecure() {
        return RoleName.ROLE_ADMIN.name();
    }
}