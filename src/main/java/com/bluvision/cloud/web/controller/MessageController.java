package com.bluvision.cloud.web.controller;

import com.bluvision.cloud.web.app.metrics.MessageCountMetrics;
import com.bluvision.cloud.web.entity.Message;
import com.bluvision.cloud.web.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class MessageController {

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MessageCountMetrics messageCountMetrics;

    @RequestMapping(value = "/messages/{id}")
    public ResponseEntity<Message> message(@PathVariable("id") Long id) {
        messageCountMetrics.increment();
        Optional<Message> msg = messageRepository.findById(id);
        return msg.isPresent() ? ResponseEntity.ok(msg.get()) : ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/messages", method = RequestMethod.POST)
    public ResponseEntity<Message> create(@RequestBody Message message) {
        return ResponseEntity.ok(messageRepository.save(message));
    }
}
