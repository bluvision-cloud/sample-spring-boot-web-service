package com.bluvision.cloud.web.controller;

import com.bluvision.cloud.web.exception.ResourceNotFoundException;
import com.bluvision.cloud.web.entity.User;
import com.bluvision.cloud.web.repository.UserRepository;
import com.bluvision.cloud.web.security.CurrentUser;
import com.bluvision.cloud.web.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
    }
}
