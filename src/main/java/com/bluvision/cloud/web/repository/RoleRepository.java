package com.bluvision.cloud.web.repository;

import com.bluvision.cloud.web.entity.Role;
import com.bluvision.cloud.web.entity.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
