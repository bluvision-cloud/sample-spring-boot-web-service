package com.bluvision.cloud.web.app.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;

@Component
public class MessageCountMetrics {
    private Counter counter;

    public MessageCountMetrics(MeterRegistry meterRegistry) {
        counter = meterRegistry.counter("message.get.requests", "messages", "ping");
    }

    public void increment() {
        counter.increment();
    }
}
